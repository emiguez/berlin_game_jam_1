require 'rubygems'
require 'gosu'

class Circle
  attr_reader :columns, :rows
  def initialize radius
    @columns = @rows = radius * 2
    lower_half = (0...radius).map do |y|
      x = Math.sqrt(radius**2 - y**2).round
      right_half = "#{"\xff" * x}#{"\x00" * (radius - x)}"
      "#{right_half.reverse}#{right_half}"
      end.join
      @blob = lower_half.reverse + lower_half
      @blob.gsub!(/./) { |alpha| "\xff\xff\xff#{alpha}"}
  end

  def to_blob
    @blob
  end
end

module ZOrder
  Background, Enemy, Player, Planet, UI = *0..3
end


class Planet
  attr_reader :x, :y, :rad

  def initialize(window, rad)
    @image = Gosu::Image.new(window, Circle.new(rad), false)
    @color = Gosu::Color.new(0xff333333)
    @rad = rad
    @x = @y = @vel_x = @vel_y = @angle = 0.0
    @vel_x = (rand * 1)
    @vel_y = (rand * 1)
    @x = (rand * 640) + rad
    @y = (rand * 480) + rad
  end

  def move
    @x += @vel_x
    @y += @vel_y
    @x %= 640
    @y %= 480
  end

  def draw
    @image.draw_rot(@x, @y, ZOrder::Planet, 0.0, 0.5, 0.5, 1, 1, @color)
  end
end

class Enemy
  def initialize(animation, left_animation, planet)
    @planet = planet
    @animation = animation
    @left_animation = left_animation
    @x = @y = @angle = 0.0
    @color = Gosu::Color.new(0xff220000)
    @color.red = rand(256 - 40) + 40
    @color.green = rand(256 - 40) + 40
    @color.blue = rand(256 - 40) + 40
    @x = planet.x + planet.rad + 8
    @y = planet.y
    @frame = 100
    @direction = (rand(100) / 2) > 50 ? 1 : -1
    @direction = 1
    @angle_on_planet =  rand(360)
  end

  def move
    @frame += 20
    @angle_on_planet += 1 * @direction
    radian = @angle_on_planet * ( Math::PI / 180)
    rad = @planet.rad + 11
    @x = @planet.x + ( rad * Math.cos(radian))
    @y = @planet.y + ( rad * Math.sin(radian))
  end

  def draw
    animation = @direction > 0 ? @animation : @left_animation
    img = animation[@frame / 100 % animation.size]
    img.draw_rot(@x, @y,
        ZOrder::Enemy, @angle_on_planet + 90)
  end
end

class Player
  attr_accessor :planet, :closer_planet, :x , :y

  def initialize(animation, left_animation, planet)
    @planet = planet
    @animation = animation
    @left_animation = left_animation
    @x = @y = @angle = 0.0
    @color = Gosu::Color.new(0xff000000)
    @color.red = rand(256 - 40) + 40
    @color.green = rand(256 - 40) + 40
    @color.blue = rand(256 - 40) + 40
    @x = planet.x + planet.rad + 8
    @y = planet.y
    @direction = 1
    @frame = 100
    @angle_on_planet =  rand(360)
    @closer_planet = nil
    @jumping = false
  end

  def jump
    @jumping = true
  end

  def go_left
    @direction = -1.0
  end
 
  def go_right
    @direction = 1.0
  end

  def move
    @frame += 20

    if @jumping
      hyp = lambda do |x| 
        return Math.sqrt(((x.x - @x) ** 2) + ((x.y - @y) ** 2))
      end
      distance = hyp.call(@closer_planet) - @closer_planet.rad
      if distance > 0
        @x += (@closer_planet.x - @x) / 50
        @y += (@closer_planet.y - @y) / 50
      else
        @jumping = false
        @planet = @closer_planet
      end
    else
      @angle_on_planet += 1 * @direction
      radian = @angle_on_planet * ( Math::PI / 180)
      rad = @planet.rad + 11
      @x = @planet.x + ( rad * Math.cos(radian))
      @y = @planet.y + ( rad * Math.sin(radian))
    end
  end

  def draw
    animation = @direction > 0 ? @animation : @left_animation
    img = animation[@frame / 100 % animation.size]
    img.draw_rot(@x, @y,
        ZOrder::Player, @angle_on_planet + 90)
  end
end

class GameWindow < Gosu::Window
  def initialize
    super(1080, 700, true)
    self.caption = "Gosu Tutorial Game"
    @animation = Gosu::Image::load_tiles(self, "media/sheep_runns_50.png" , 50, 50, false)
    @enemy_animation =  Gosu::Image::load_tiles(self, "media/dino_50.png" , 50, 50, false)
    @left_enemy_animation =  Gosu::Image::load_tiles(self, "media/dino_50_left.png" , 50, 50, false)
    @left_animation = Gosu::Image::load_tiles(self, "media/sheep_runns_50_left.png" , 50, 50, false)
    @background_image = Gosu::Image.new(self, "media/Space.png", true)
    @planets = []
    @enemies = []
    1..4.times do |x|
      p = Planet.new(self, 50)
      @enemies << Enemy.new(@enemy_animation, @left_enemy_animation, p)
      @planets <<  p
    end
    pl = Planet.new(self, 50)
    @planets << pl
    @player = Player.new(@animation, @left_animation, pl)
  end


  def draw
    @background_image.draw(0, 0, ZOrder::Background)
    @planets.each { |p| p.draw }
    @enemies.each { |u| u.draw }
    @player.draw
  end


  def update
    if button_down? Gosu::KbLeft or button_down? Gosu::GpLeft 
      @player.go_left
    end
    if button_down? Gosu::KbRight or button_down? Gosu::GpRight
      @player.go_right
    end
    if button_down? Gosu::KbUp or button_down? Gosu::GpUp
      @player.jump
    end
    @enemies.each { |x| x.move }
    @player.move
    @planets.each { |x| x.move }
    other_planets = @planets.reject { |x| x == @player.planet }
    hyp = lambda do |x| 
      y = @player
      return Math.sqrt(((x.x - y.x) ** 2) + ((x.y - y.y) ** 2))
    end

    @player.closer_planet = other_planets.sort do |x, y|
      if  hyp.call(x) < hyp.call(y)
        -1
      elsif hyp.call(y) < hyp.call(x)
        1
      else
        0
      end
    end.first
  end
end

window = GameWindow.new
window.show
